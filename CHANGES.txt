v1.2.0.0,       2013 Jul 26 --- Should be much better handling of unicode, internally and externally. I think.
                                Added --in-enc and --out-enc options, so everything should be unicode internally.

v1.1.0.0,       2013 Jul 26 --- Added a few functions, notable file-get-contents and file-put-contents

v1.0.1.0,       2013 Apr 08 --- Some fixed to handle utf-8 better, and might be more compatible with older pythons.

v0.3.0.2,       2013 Jan 16 --- Forgot to delete the "dev" tag from the previous releases.

v0.3.0.1-dev,   2013 Jan 16 --- Added some additional instructions to INSTALL.txt

v0.3.0.0-dev,   2013 Jan 16 --- Primarily just improved python distribution and packaging.

v0.1.0.0-dev,   2012 Aug 13 --- Initial development pre-release.

